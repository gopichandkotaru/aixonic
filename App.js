import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, FlatList, Image, TextInput} from 'react-native';
  
export default class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[]
     };
   }
 
  componentDidMount(){
    fetch("https://run.mocky.io/v3/82f1d43e-2176-4a34-820e-2e0aa4566b5c")
    .then(response => response.json())
    .then((responseJson)=> {
      this.setState({
       dataSource: responseJson
      })
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }
 
    render(){
     return(
      <View style={{backgroundColor:'grey'}}>
        <View style={{flexDirection:'row',borderWidth:1,borderColor:'orange',height:60,backgroundColor:'orange',alignItems:'center',paddingHorizontal:10}}>
          <Text style={{fontSize:25,color:'white',width:'75%'}}>Assigned to Me</Text>
          <Image style={{width:25,height:25,marginRight:'5%',tintColor:'white'}} source = {require('./images/refresh.png')} />
          <Image style={{width:30,height:30,marginRight:'5%',tintColor:'white'}} source = {require('./images/location.png')} />
        </View>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <View style={{flexDirection:'row',borderWidth:1,borderColor:'white',margin:10,height:40,  backgroundColor:'white',alignItems:'center',width:'70%',paddingHorizontal:10,borderRadius:10}}>
            <Image style={{width:20,height:20,marginRight:'5%'}} source = {require('./images/search.png')} />
            <TextInput style={{fontSize:15}} placeholder="Search Task"/>
          </View>
          <View style={{flexDirection:'row',backgroundColor:'orange',height:40,alignItems:'center',paddingHorizontal:5,borderRadius:10}}>
            <Text style={{fontSize:15,color:'white',paddingHorizontal:5}}>Filter</Text>
            <Image style={{width:20,height:20,marginRight:'5%',tintColor:'white'}} source = {require('./images/filter.png')} />
          </View>
        </View>
        <FlatList
          padding ={10}
          data={this.state.dataSource}
          renderItem={({item}) => 
          <View style={{padding:5,height: 200,backgroundColor:'white',borderWidth:1,borderColor:'green',  marginBottom:'1%'}}>
            <View style={{borderBottomWidth:1,borderColor:'grey',flexDirection:'row',padding:5}}>
              <View style={{backgroundColor:'grey',borderRadius:50,width:'15%',marginRight:'2%'}}>
                <Text style={{fontSize:30,padding:5,textAlign:'center'}}>W</Text>
              </View>
              <View style={{width:'83%',alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <View>
                    <Text style={{}}>{item.title}</Text>
                  </View>
                  <View style={{backgroundColor:'orange',paddingVertical:5,borderRadius:10,width:90}}>
                    <Text style={{textAlign:'center'}}>{item.status}</Text>
                  </View>
                </View>
                <Text style={{}}>{item.subtitle}</Text>
              </View>
            </View>
            <View style={{flexDirection:'row',borderBottomWidth:1,borderColor:'grey',padding:5}}>
              <Image style={{width:20,height:20,marginRight:'5%'}} source = {require('./images/events.png')} />
              <Text style={{fontSize:15}}>Created: {item.created}</Text>
            </View>
            <View style={{flexDirection:'row',padding:5}}>
              <Image style={{width:20,height:20,marginRight:'5%',tintColor:'grey'}} source = {require('./images/menu.png')} />
              <Text style={{}}>{item.short_desc}</Text>
            </View>
            <View style={{flexDirection:'row',padding:5}}>
              <Image style={{width:20,height:20,marginRight:'5%'}} source = {require('./images/arts.png')} />
              <Text style={{}}>{item.long_desc}</Text>
            </View>
          </View>
        }/>
      
     </View>
     )}
}
 